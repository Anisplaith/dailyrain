# DailyRain | EJCM

DailyRain é um diário/bloco de anotações para desktop. É destinado para o usuário cadastrado escrever, armazenar e ler passagens escritas por si mesmo. Será suportado rich text e fotos. Esse projeto foi feito para o PSI de Projetos da EJCM.
 
**Status do Projeto** : Terminado

![Badge](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)
![Badge](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white)
![Badge](https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB)
![Badge](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)
![Badge](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)
![Badge](https://img.shields.io/badge/Express.js-000000?style=for-the-badge&logo=express&logoColor=white)
![Badge](https://img.shields.io/badge/Sequelize-52B0E7?style=for-the-badge&logo=Sequelize&logoColor=white)
![Badge](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)


 
## Tabela de Conteúdo

 1. [Tecnologias utilizadas](#tecnologias-utilizadas)
 2. [Instalação](#instalação)
 3. [Configuração](#configuração)
 4. [Uso](#uso)
 5. [Testes](#testes)
 6. [Arquitetura](#arquitetura)
 7. [Autores](#autores)
 
## Tecnologias utilizadas

Essas são as frameworks e ferramentas que você precisará instalar para desenvolver esse projeto:

 - React JS, versão 18.2.0
 - Node, versão 16.14.2

Outras tecnologias interessantes que foram implementadas:

 - unsplash-js, versão 7.0.15
 - API unsplash
 - node-fetch, versão 2.6.1
 - react-draft-wysiwyg, versão 1.15.0
 - react-toastify, versão 9.1.1

## Instalação 

* Na pasta 'back' para instalar as dependências rode no terminal:

``` bash
$ npm install
```

* Na pasta 'front' para instalar as dependências rode no terminal:

``` bash
$ yarn install
```

## Configuração

* Na pasta do 'back' rode no terminal:


``` bash
$ cp .env.example .env
```
``` bash
$ npm run migrate
```
``` bash
$ npm run keys
```
 
## Uso

* Na pasta 'back', para rodar o servidor: 

``` bash
$ npm run dev
```
* Na pasta 'front', para rodar o projeto: 

``` bash
$ yarn start
```
## Arquitetura

- Modelagem DB: https://media.discordapp.net/attachments/863377930205003787/1082075328576487424/Modelagem_DB_-_DailyRain.png?width=961&height=349

- Alguns assets do figma: https://www.figma.com/file/LOqWl3qFdVzdm8wqPYEhlX/DailyRain?node-id=0%3A1&t=wgFVLatoiYiVoaVe-0


## Autores

> Período: (15/02 - 05/03)
* Gerente - Rayane DB
* Tech Lead - Rayane DB
* Dev Front-end - Rayane DB
* Dev Back-end - Rayane DB  



## Última atualização: 07/03/2023

